module Chapter1 where

mysquare :: Int -> Int
mysquare x = x * x

sum_of_squares :: Int -> Int -> Int
sum_of_squares x y = mysquare x + mysquare y

_module :: Int -> Int
_module x | x > 0   = x
          | x < 0   = -x
          | x == 0  = 0

_module_v2 :: Int -> Int
_module_v2 x = if x >= 0 then x else (-x)

is_consonant :: Char -> Bool
is_consonant x = x /= 'a' && x /= 'o' && x /= 'u' && x /= 'i' && x /= 'e'

is_vowel :: Char -> Bool
is_vowel x = not (is_consonant x)

sum_larger :: Int -> Int -> Int -> Int
sum_larger x y z = if (x < y) then
  if (x < z) then y * y + z * z else y * y + x * x
  else if (y < z) then x * x + z * z else y * y + x * x
  