-- Type classes are similar to interfaces in other languages
class Describable a where
  describe :: a -> String

data Writing = Article String | Novel 

-- This is similar to 'Writing implements Describable' in Java:
instance Describable Writing where
  describe (Article title) = "a newspaper article about " ++ title
  describe Novel = "a novel"
