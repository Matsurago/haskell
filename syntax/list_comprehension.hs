--                        [ f(x)  | x <- S    , P(x)           ]
oddNumbersDivisibleBy n = [ 2 * x | x <- [1..], x `mod` n == 0 ]

-- can draw elements from multiple lists of different length
-- it behaves like two nested 'for' loops
-- [ 1, -1, 2, -2, 3, -3, ... ]
alternatingSign = [ x * y | x <- [1..], y <- [1, -1] ]

-- nested comprehension 
-- applies f to all elements in a list of lists, such as [[1,2,3], [4,5], [6]]
mapNested f xxs = [ [f x | x <- xs] | xs <- xxs ]

-- with pattern matching
-- [Just 2, Nothing, Just 3] -> [Just 2, Just 3]
removeNulls :: [Maybe a] -> [a]
removeNulls xs = [ x | (Just x) <- xs ]
