-- Order of arguments should be from more general to less general
makeRequestUrl :: String -> String -> String -> String -> String
makeRequestUrl host apiKey resource id =
  foldl1 (++) [host, "/", resource, "/", id, "?token=", apiKey]

-- Partial application --
makeExampleComUrl = makeRequestUrl "https://example.com"
makeMyTokenExampleComUrl = makeExampleComUrl "xyz12345"

main :: IO()
main = do
  putStrLn $ makeMyTokenExampleComUrl "users" "3"
