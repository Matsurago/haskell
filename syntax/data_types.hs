-- type aliases (can use interchangeably with String)
type FirstName = String
type LastName = String
type MiddleName = String

-- thin wrapper around a type (defines a new type)
newtype ClinicId = ClinicId Int

-- create a new sum type
data Sex = Male | Female  deriving (Show, Eq)

data RhType = Pos | Neg
data ABOType = A | B | AB | O  deriving (Show, Eq)
data BloodType = BloodType ABOType RhType

-- RhType implements Show
instance Show RhType where
  show Pos = "+"
  show Neg = "-"

instance Show BloodType where
  show (BloodType abo rh) = show abo ++ show rh

canDonateTo :: Patient -> Patient -> Bool
canDonateTo donor receiver = compatibleBlood (bloodType donor) (bloodType receiver)
  where
    compatibleBlood :: BloodType -> BloodType -> Bool -- NOTE: ignoring Rh
    compatibleBlood (BloodType O _) _ = True   -- O can donate to anyone
    compatibleBlood _ (BloodType AB _) = True  -- AB can receive from anyone
    compatibleBlood (BloodType A _) (BloodType A _) = True
    compatibleBlood (BloodType B _) (BloodType B _) = True
    compatibleBlood _ _ = False

-- a type with multiple data constructors
data Name = Name FirstName LastName
          | NameWithMiddle FirstName MiddleName LastName

instance Show Name where
  show (Name firstName lastName) = firstName ++ " " ++ lastName
  show (NameWithMiddle firstName middleName lastName) = firstName ++ " " ++ middleName ++ " " ++ lastName

-- a type using record syntax (aka builder)
data Patient = Patient {
  clinicId :: ClinicId,
  name :: Name,
  sex  :: Sex,
  age  :: Int,
  height :: Int,
  weight :: Int,
  bloodType :: BloodType
}

patientSummary :: Patient -> String
patientSummary patient = "**************\n" ++
  "Patient Id: " ++ show patientId ++ "\n" ++
  "Patient Name: " ++ show (name patient) ++ "\n" ++
  "Sex: " ++ show (sex patient) ++ "\n" ++
  "Age: " ++ show (age patient) ++ "\n" ++
  "Height: " ++ show (height patient) ++ " cm\n" ++
  "Weight: " ++ show (weight patient) ++ " kg\n" ++
  "Blood Type: " ++ show (bloodType patient) ++ "\n" ++
  "**************\n"
  where (ClinicId patientId) = clinicId patient

main :: IO()
main = do
  let jackie = Patient {
    clinicId = ClinicId 1000,
    name = NameWithMiddle "Jackie" "Harvard" "Chan",
    sex = Male,
    height = 167,
    weight = 90,
    bloodType = BloodType O Pos,
    age = 38
  }
  putStr $ patientSummary jackie

  let anna = Patient {
    clinicId = ClinicId 1001,
    name = Name "Anna" "Smith",
    sex = Female,
    height = 172,
    weight = 67,
    bloodType = BloodType AB Pos,
    age = 21
  }  
  putStr $ patientSummary anna
  
  putStrLn $ "Jackie can donate to Anna: " ++ show (canDonateTo jackie anna)
  putStrLn $ "Anna can donate to Jackie: " ++ show (canDonateTo anna jackie)
