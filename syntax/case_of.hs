numberName n = case n of
           0 -> "Zero"
           1 -> "One"
           2 -> "Two"
           3 -> "Three"
           4 -> "Four"
           _ -> "Greater than four"
           
