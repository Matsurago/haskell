-- pattern matching can be used in lambdas
-- WARNING: fail to match leads to a runtime error
mapMin :: Ord a => [(a, a)] -> [a]
mapMin = map (\(x, y) -> min x y)

-- Note: the same function can be rewritten more concisely as:
mapMin' :: Ord a => [(a, a)] -> [a]
mapMin' = map (uncurry min)


-- match "as pattern" to name the combined value
sorted :: Ord a => [a] -> Bool
sorted []  = True
sorted [_] = True
sorted (x:r@(y:ys)) = x < y && sorted r
