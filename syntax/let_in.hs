qe_roots a b c = 
    let
        det = sqrt (b*b - 4*a*c)
        x1 = ((-b) + det) / (2*a)
        x2 = ((-b) - det) / (2*a)
    in
        [x1, x2]
