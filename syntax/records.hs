data Person = Person {
    firstName :: String,
    lastName :: String,
    age :: Int
} deriving Show

-- can create an object using field names
-- this object can further be used as a prototype for new objects:
-- as in:  samplePerson { age = 20 }
samplePerson :: Person
samplePerson = Person { firstName = "John", lastName = "Smith", age = 18 }

-- can use in pattern matching omitting some fields
greet :: Person -> String
greet Person { firstName = fn } = "Hi " ++ fn

-- updating a field creates a new object
setName :: Person -> String -> Person
setName p name = p { firstName = name }
