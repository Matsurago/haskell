calculateChange owed given = 
    if change > 0
    then change
    else 0
    where 
        change = given - owed

sumSquareOrSquareSum x y = 
    let sumSquare = (x^2 + y^2)
        squareSum = (x+y)^2
    in
        if sumSquare > squareSum 
        then sumSquare 
        else squareSum
