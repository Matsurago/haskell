-- ["John", "Will", "Anya"]  ->  [(1, "John"), (2, "Will"), (3, "Anya")]
enumerate = zip [1..]

-- [2, 3] [1, 5] -> 2*1 + 3*5 = 17
scalarProduct :: Num a => [a] -> [a] -> a
scalarProduct xs ys = sum $ zipWith (*) xs ys

-- window (size = 2)
-- [1, 2, 3, 4] -> [(1, 2), (2, 3), (3, 4)]
windowed :: [a] -> [(a, a)]
windowed xs = zip xs (tail xs)

-- isSorted by using windowed
isSorted :: Ord a => [a] -> Bool
isSorted xs = and [ x <= y | (x, y) <- windowed xs ]
