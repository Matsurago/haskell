import Data.List (foldl')

-- Right fold
-- Useful when creating a list in the process of folding
-- Can work with infinite lists (!)
-- Equivalent to:  f x1 (f x2 (f x3 e))   for   [x1, x2, x3]
mapR f = foldr (\x acc -> (f x) : acc) []

-- Left fold
-- Very slow with lists
-- Equivalent to:  f (f (f e x1) x2) x3   for   [x1, x2, x3]
mapL f = foldl (\acc x -> acc ++ [f x]) []

-- foldl may cause stack overflow on long lists, because the computation is deferred
-- use foldl' from Data.List for non-lazy computation
sumTenMillion = foldl' (+) 0 (replicate 10000000 1)

-- foldl1 (foldr1) function takes the first (last) element in the list as identity
-- WARNING: throws exception if the list is empty
maximum' :: Ord a => [a] -> a
maximum' = foldl1 max

-- reverse = fold + flip
reverse' :: [a] -> [a]
reverse' = foldl (flip (:)) []

-- functions scanl, scanr, scanl1, scanr1 work the same as folds,
-- except they produce all intermediate results as well
-- ex: allSums [1,4,2] ->  [0,1,5,7]
allSums = scanl (+) 0 
