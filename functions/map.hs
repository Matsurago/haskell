-- flatMap via map
flatMap1 :: (a -> b) -> [[a]] -> [b]
flatMap1 f = concat . map (map f)

-- flatMap via map, version 2
flatMap2 :: (a -> b) -> [[a]] -> [b]
flatMap2 f = concat . (map . map) f
