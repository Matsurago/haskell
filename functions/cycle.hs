makeBins n = zip (cycle [1..n])

main :: IO()
main = do
  let files = ["1.txt", "2.txt", "3.txt", "4.txt", "5.txt", "6.png", "7.png"]
  print $ makeBins 3 files
