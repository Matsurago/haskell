-- sequence: t (m a) -> m (t a) 
-- where t is Traversable and m is Monad
--
-- Converts a list of monads into a monad containing a list
import System.Random (Random, randomRIO)


main :: IO ()
main = do
    -- m = Maybe
    -- [Maybe Int] -> Maybe [Int] 
    print $ sequence [Just 1, Just 2, Nothing]  -- Nothing
    print $ sequence [Just 1, Just 2, Just 3]  -- Just [1, 2, 3]

    -- m = Either : leaves only the first error
    -- [Either String Int] -> Either String [Int]
    print $ sequence [Right 42, Left "err 1", Left "err 2"]  -- Left "err 1"
    print $ sequence [Right 42, Right 43 :: Either String Int]  -- Right [42, 43] 

    -- m = Random
    -- [IO Int] -> IO [Int]
    sequence [randomRIO (1, 6), randomRIO (1, 6) :: IO Int] >>= print  -- IO [?, ?]

    -- m = List 
    -- [[Int]] -> [[Int]]     -- because both t and m are lists
    print $ sequence [[1, 2], [3, 4, 5]]  -- [[1,3],[1,4],[1,5],[2,3],[2,4],[2,5]]
