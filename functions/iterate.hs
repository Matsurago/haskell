-- iterate:
-- given f :: a -> a and x :: a
-- produces a list [a, f a, f (f a), f (f (f a)), ...]

main :: IO()
main = do
    -- powers of 2
    print $ take 10 $ iterate (*2) 1

    -- squares
    print $ take 5 $ iterate (\n -> n * n) 2

