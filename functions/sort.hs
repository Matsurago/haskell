import Data.List

names = [("Vasya", "Pupkin"), ("Alex", "Boldwin"), ("Chris", "Laundry"), ("Arnold", "Strong")]

compareSurnames :: ([Char], [Char]) -> ([Char], [Char]) -> Ordering
compareSurnames tuple1 tuple2 =
                if surnameComparisonResult /= EQ
                then surnameComparisonResult
                else compare name1 name2
     where surnameComparisonResult = compare surname1 surname2
           surname1 = snd tuple1
           surname2 = snd tuple2
           name1 = fst tuple1
           name2 = fst tuple2

main :: IO()
main = do
     print (sort names)
     print (sortBy compareSurnames names)
