-- Functor, Applicative, and Monad type classes
-- allow to apply regular functions to values embedded 
-- in some kind of a structure (also called 'context').
--
-- Having a function f, there are 4 possible signature mismatches
-- when we have an argument of type 'Context a' and want the result of type 'Context b':
-- 1) f :: a -> b               solved by Functor: <$>
-- 2) f :: Context a -> b       solved by Applicative
-- 3) Context (f :: a -> b)     solved by Applicative: <*> 
-- 4) f :: a -> Context b       solved by Monad: >>=

data Person = Person String String Int deriving (Eq, Show)

main = do
    -- [ Functor ]
    -- Case 1: want to apply f :: a -> b, but have an argument of type 'Context a'
    -- Example: use fmap (<$>) to apply '(+1) :: Int -> Int' to arg of type 'Maybe Int'
    print $ (+1) <$> (Just 7)
    print $ fmap (+1) (Just 7)  -- same as above
    print $ fmap (+1) [1, 2, 3]
    print $ fmap (+1) (Right 7 :: Either String Int)
    print $ fmap (+1) (Left "error" :: Either String Int)
    -- in other words, we apply (lift) the function over some structure or layer,
    -- leaving that layer intact.

    -- mapping over multiple layers
    print $ (fmap . fmap) (+1) [[1, 2], [3, 4, 5]]
    print $ (fmap . fmap . fmap) (+1) [Just [1, 2], Nothing, Just [6, 7, 8]]
    putStrLn ""

    -- [ Applicative ]
    -- Case 3: want to apply 'Context (f :: a -> b)' to an argument of type 'Context a'
    -- to get a result of type 'Context b'
    -- Example: use <*> to apply [a -> b] to [Int]
    print $ [(+1), (*3)] <*> [1, 2, 3]
    print $ (Just (+1)) <*> Just 7
    
    -- applying fmap (Functor) to a binary function results in 
    -- a partially applied function in Context 
    let foo = fmap (+) (Just 7)  -- foo :: Just (+7)
    -- so we need Applicative to apply it to the second argument
    print $ foo <*> (Just 8)
    -- or, in one-liner:
    print $ (+) <$> (Just 7) <*> (Just 8)
    -- same is achieved with Applicative liftA2:
    print $ liftA2 (+) (Just 7) (Just 8)
    -- List Context can be considered as a choice between items
    -- this creates all possible combinations
    print $ pure Person <*> ["John", "Helga"] <*> ["Smith", "Brown"] <*> [18, 24, 30]
    
    -- [ Monad ]
    -- Case 4: want to apply 'f :: a -> Context b' to an argument of type 'Context a'
    -- to get a result of type 'Context b'
    let echo n c = take n . repeat $ c  -- echo :: a -> [a]
    print $ [1, 2, 3] >>= (echo 3)      -- [1, 1, 1, 2, 2, 2, 3, 3, 3]
