import Control.Monad (guard)

-- produces all combinations of values in two lists
-- that satisfy predicate p
setProduct :: [a] -> [b] -> (a -> b -> Bool) -> [(a, b)]
setProduct xs ys p = do
    x <- xs  -- treat a list as a single value
    y <- ys
    guard $ p x y  -- acts like filter
    return (x, y)


main :: IO ()
main = do
    let nums = [1..5]
    let decart = setProduct nums nums (\x y -> x + y < 8)
    print $ decart
