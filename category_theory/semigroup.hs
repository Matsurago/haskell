import Data.Semigroup

data Color = Red |
             Yellow |
             Blue |
             Green |
             Purple |
             Orange |
             Brown |
             Transparent deriving (Show, Eq)

-- A semigroup is a set with an associative binary operation.
-- A monoid is a semigroup with the identity element
instance Semigroup Color where
  (<>) Red Blue = Purple
  (<>) Blue Red = Purple
  (<>) Yellow Blue = Green
  (<>) Blue Yellow = Green
  (<>) Yellow Red = Orange
  (<>) Red Yellow = Orange
  (<>) a Transparent = a
  (<>) Transparent a = a
  (<>) a b | a == b = a
           | all (`elem` [Red, Blue, Purple]) [a, b] = Purple
           | all (`elem` [Blue, Yellow, Green]) [a, b] = Green
           | all (`elem` [Red, Yellow, Orange]) [a, b] = Orange
           | otherwise = Brown

instance Monoid Color where
  mempty = Transparent
  mappend = (<>)
