# Haskell
Code snippets in Haskell.

Each file can be loaded in GHCi as usual:
```
ghci
:l <filename>
```

Some snippets depend on external libraries:
- [dev-haskell/vector](https://hackage.haskell.org/package/vector)
- [dev-haskell/hspec](https://hspec.github.io/)

To install libraries without Stack, use:
```
cabal install --lib base
cabal install --lib random
cabal install --lib vector
cabal install --lib hspec
cabal install --lib QuickCheck
```

## Useful Libraries

| Purpose | Library |
|---------|---------|
| Web apps | Yesod, Scotty |
| JSON | Aeson |
| 2D vector graphics | Gloss |
