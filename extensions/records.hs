{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE RecordWildCards #-}

data Person = Person {
    firstName :: String,
    lastName :: String
} deriving Show

-- automatically creates a variable for a field (NamedFieldPuns)
greet :: Person -> String
greet Person { firstName } = "Hi " ++ firstName

-- automatically creates variables for all fields (RecordWildCards)
greet2 :: Person -> String
greet2 Person { .. } = "Hello " ++ firstName
