{-# LANGUAGE ViewPatterns #-}

-- View patterns generalize the idea guards
-- by allowing to apply to a value any (even non-Boolean) function

-- a utility function
trim :: [Char] -> [Char]
trim = dropWhile (== ' ')

-- view patterns example (use like:  auth "    admin")
auth :: [Char] -> [Char]
auth (trim -> "admin") = "authenticated"
auth _ = "403 forbidden"
