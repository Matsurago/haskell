import Test.Hspec
import Test.QuickCheck
import Tokenizer

main = hspec $ do
  describe "Text Utilities Test" $ do

    it "tokenizes by space" $ do
      tokenize "a white cat" " " `shouldBe` ["a"," ","white"," ","cat"]

    it "tokenizes by dot and comma" $ do
      tokenize "720,109.00" ".," `shouldBe` ["720",",","109",".","00"]
