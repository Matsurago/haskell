-- Dijkstra Two-Stack Algorithm for Expression Evaluation --
module ExpressionDijkstra where

import Tokenizer


type MathOperator = String

mathOperators :: [MathOperator]
mathOperators = ["+", "-", "*", "/", "sqrt"]

isMathOperator :: String -> Bool
isMathOperator token = token `elem` mathOperators

isBinaryOperator :: MathOperator -> Bool
isBinaryOperator "sqrt" = False
isBinaryOperator _ = True

evaluate :: String -> Double
evaluate input = go (tokenize input "+-*/()") [] []
  where
    -- input tokens /  numbers  /   operators 
    go :: [String] -> [Double] -> [MathOperator] -> Double
    go [] numbers operators = head numbers  -- no more input, return topmost number from numbers' stack

    -- ignore left parenthesis
    go (token:rest) numbers operators | token == "(" =
      go rest numbers operators

    -- on right parenthesis, apply the topmost operator on the operators' stack
    -- to the topmost numbers on the numbers' stack,
    -- and put the result back on the numbers' stack
    go (token:rest) (x:numbers) (op:operators) | token == ")" =
      go rest (intermediateResult : remainingNumbers) operators
        where
          remainingNumbers =   if isBinaryOperator op
                               then tail numbers
                               else numbers
          intermediateResult = if isBinaryOperator op
                               then applyBinaryOperator op (head numbers) x
                               else applyUnaryOperator  op x

    -- push an operator onto the operators' stack
    go (token:rest) numbers operators | isMathOperator token =
      go rest numbers (token : operators)

    -- push a number onto the numbers' stack
    go (token:rest) numbers operators | otherwise =
      go rest ((read token :: Double) : numbers) operators

applyBinaryOperator :: MathOperator -> Double -> Double -> Double
applyBinaryOperator op x y 
  | op == "+" = x + y
  | op == "-" = x - y
  | op == "*" = x * y
  | op == "/" = x / y
  | otherwise = error $ "Unknown binary operator: " ++ op

applyUnaryOperator :: MathOperator -> Double -> Double
applyUnaryOperator op x 
  | op == "sqrt" = x**0.5
  | otherwise = error $ "Unknown unary operator: " ++ op
