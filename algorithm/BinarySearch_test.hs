import Data.Vector (fromList)
import Test.Hspec
import Test.QuickCheck
import BinarySearch

main = hspec $ do
  describe "Binary Search Test" $ do

    let list_odd = fromList [34, 48, 76, 99, 112, 278, 950]

    it "returns 0 given 34 and [34, 48, 76, 99, 112, 278, 950]" $ do
      binary_search list_odd 34 `shouldBe` Just 0

    it "returns 1 given 48 and [34, 48, 76, 99, 112, 278, 950]" $ do
      binary_search list_odd 48 `shouldBe` Just 1

    it "returns 2 given 76 and [34, 48, 76, 99, 112, 278, 950]" $ do
      binary_search list_odd 76 `shouldBe` Just 2

    it "returns 3 given 99 and [34, 48, 76, 99, 112, 278, 950]" $ do
      binary_search list_odd 99 `shouldBe` Just 3

    it "returns 4 given 112 and [34, 48, 76, 99, 112, 278, 950]" $ do
      binary_search list_odd 112 `shouldBe` Just 4

    it "returns 5 given 278 and [34, 48, 76, 99, 112, 278, 950]" $ do
      binary_search list_odd 278 `shouldBe` Just 5

    it "returns 6 given 950 and [34, 48, 76, 99, 112, 278, 950]" $ do
      binary_search list_odd 950 `shouldBe` Just 6

    it "returns Nothing given 113 and [34, 48, 76, 99, 112, 278, 950]" $ do
      binary_search list_odd 113 `shouldBe` Nothing


    let list_even = fromList [34, 48, 76, 99, 112, 278]

    it "returns 0 given 34 and [34, 48, 76, 99, 112, 278]" $ do
      binary_search list_even 34 `shouldBe` Just 0

    it "returns 1 given 48 and [34, 48, 76, 99, 112, 278]" $ do
      binary_search list_even 48 `shouldBe` Just 1

    it "returns 2 given 76 and [34, 48, 76, 99, 112, 278]" $ do
      binary_search list_even 76 `shouldBe` Just 2

    it "returns 3 given 99 and [34, 48, 76, 99, 112, 278]" $ do
      binary_search list_even 99 `shouldBe` Just 3

    it "returns 4 given 112 and [34, 48, 76, 99, 112, 278]" $ do
      binary_search list_even 112 `shouldBe` Just 4

    it "returns 5 given 278 and [34, 48, 76, 99, 112, 278]" $ do
      binary_search list_even 278 `shouldBe` Just 5

    it "returns Nothing given 113 and [34, 48, 76, 99, 112, 278]" $ do
      binary_search list_even 113 `shouldBe` Nothing


    let list_empty = fromList ([] :: [Int])

    it "returns Nothing given 0 and []" $ do
      binary_search list_empty 0 `shouldBe` Nothing
