import Test.Hspec
import Test.QuickCheck
import ExpressionDijkstra

main = hspec $ do
  describe "Dijkstra Two-Stack Algorithm for Expression Evaluation Test" $ do

    it "returns 101 given (1+((2+3)*(4*5)))" $ do
      evaluate "(1+((2+3)*(4*5)))" `shouldBe` 101

    it "returns 1.618033988749895 given ((1+sqrt(5.0))/2.0)" $ do
      evaluate "((1+sqrt(5.0))/2.0)" `shouldBe` 1.618033988749895
