import Test.Hspec
import Test.QuickCheck
import TestSortUtilities
import MergeSort

sort :: Ord a => [a] -> [a]
sort = msort

main :: IO ()
main = hspec $ do
  describe "Merge Sort Test" $ do

    it "sorts empty list" $ do
      sort ([] :: [Int]) `shouldBe` []

    it "sorts singleton list" $ do
      sort [1] `shouldBe` [1]

    it "sorts [1, 2]" $ do
      sort [1, 2] `shouldBe` [1, 2]

    it "sorts [2, 1]" $ do
      sort [2, 1] `shouldBe` [1, 2]

    it "sorts [1, 2, 3]" $ do
      sort [1, 2, 3] `shouldBe` [1, 2, 3]

    it "sorts [3, 1, 2]" $ do
      sort [3, 1, 2] `shouldBe` [1, 2, 3]

    it "sorts [3, 2, 1]" $ do
      sort [3, 2, 1] `shouldBe` [1, 2, 3]

    it "sorts [9, 0, 1, 4, 8, 7, 5, 6, 3, 2]" $ do
      sort [9, 0, 1, 4, 8, 7, 5, 6, 3, 2] `shouldBe` [0 .. 9]

    it "sorts [1, 2, 2, 1, 3, 2, 3, 3, 1]" $ do
      sort [1, 2, 2, 1, 3, 2, 3, 3, 1] `shouldBe` [1, 1, 1, 2, 2, 2, 3, 3, 3]

    it "sorts a random list" $ do
      xs <- randomList 1000
      isSorted (sort xs) `shouldBe` True 
