module Fibonacci where

my_fib :: Int -> Integer
my_fib n = go 0 1 n
  where
    go _ _ 0 = 0
    go _ _ 1 = 1
    go prev2 prev 2 = prev2 + prev
    go prev2 prev n = go prev (prev2+prev) (n-1)
