module QuickSort where

qsort :: Ord a => [a] -> [a]
qsort [] = []
qsort (x:xs) = qsort less ++ [x] ++ qsort greater
  where
    less    = [a | a <- xs, a <= x]
    greater = [b | b <- xs, b > x]
