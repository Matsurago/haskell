import Test.Hspec
import Test.QuickCheck
import GreatestCommonDivisor

main :: IO ()
main = hspec $ do
  describe "Greater Common Divisor Algorithm Test" $ do

    it "gcd(1, 4) is 1" $ do
      my_gcd 1 4 `shouldBe` 1

    it "gcd(6, 3) is 3" $ do
      my_gcd 6 3 `shouldBe` 3

    it "gcd(21, 35) is 7" $ do
      my_gcd 21 35 `shouldBe` 7
