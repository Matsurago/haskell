module BinarySearch where

import Data.Vector (Vector, (!))


binary_search :: Ord a => Vector a -> a -> Maybe Int
binary_search elements x = go elements x 0 (length elements - 1)
  where
    go :: Ord a => Vector a -> a -> Int -> Int -> Maybe Int
    go elements x low high
      | low > high = Nothing
      | x > mid    = go elements x (mid_index + 1) high
      | x < mid    = go elements x low (mid_index - 1)
      | otherwise  = Just mid_index
      where
        mid = elements ! mid_index
        mid_index = (low + high) `div` 2
