module Tokenizer where


tokenize :: String -> [Char] -> [String]
tokenize (char:string) separators = go (char:string) separators ""
  where
    go :: String -> [Char] -> String -> [String]
    go [] _ acc | acc == "" = []
    go [] _ acc | otherwise = [acc]
    go (char:string) separators acc =
      if char `elem` separators
      then
        if acc == ""
        then [char] : (go string separators "")
        else [acc, [char]] ++ (go string separators "")
      else go string separators (acc ++ [char])
