module GreatestCommonDivisor where

my_gcd :: Int -> Int -> Int
my_gcd x y = if remainder == 0
             then y
             else my_gcd y remainder
  where remainder = x `mod` y
