module InsertionSort where

-- Inserts a value into a sorted list
insert :: (Ord a) => a -> [a] -> [a]
insert a [] = [a]
insert a r@(x:xs)
  | a <= x = a:r
  | otherwise = x:(insert a xs)

isort :: (Ord a) => [a] -> [a]
isort [] = []
isort (x:xs) = insert x (isort xs)
