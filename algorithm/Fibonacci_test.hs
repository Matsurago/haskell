import Test.Hspec
import Test.QuickCheck
import Fibonacci

main = hspec $ do
  describe "Fibonacci Number Sequence Test" $ do

    it "returns 0 given 0" $ do
      my_fib 0 `shouldBe` 0

    it "returns 1 given 1" $ do
      my_fib 1 `shouldBe` 1

    it "returns 1 given 2" $ do
      my_fib 2 `shouldBe` 1

    it "returns 2 given 3" $ do
      my_fib 3 `shouldBe` 2

    it "returns 3 given 4" $ do
      my_fib 4 `shouldBe` 3

    it "returns 5 given 5" $ do
      my_fib 5 `shouldBe` 5

    it "returns 8 given 6" $ do
      my_fib 6 `shouldBe` 8

    it "returns 12586269025 given 50" $ do
      my_fib 50 `shouldBe` 12586269025

    it "returns 222232244629420445529739893461909967206666939096499764990979600 given 300" $ do
      my_fib 300 `shouldBe` 222232244629420445529739893461909967206666939096499764990979600
      
