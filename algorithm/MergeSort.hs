module MergeSort where

-- Merges two sorted lists
merge :: Ord a => [a] -> [a] -> [a]
merge xs [] = xs
merge [] ys = ys
merge a@(x:xs) b@(y:ys) = if x <= y then x:(merge xs b) else y:(merge a ys)

-- Merge sort
msort :: Ord a => [a] -> [a]
msort [] = []
msort [x] = [x]
msort xs = merge (msort (take m xs)) (msort (drop m xs))
  where m = length xs `div` 2
