module TestSortUtilities where

import Control.Monad.IO.Class (MonadIO)
import System.Random (Random)
import System.Random.Stateful (randomRM, globalStdGen)


-- A random list of floats in [0..1] of the given length n
randomList :: MonadIO m => Int -> m [Float]
randomList n
  | n <= 0 = return []
  | otherwise = sequence . take n . randomListInf $ (0.0, 1.0)


-- An infite random list
randomListInf :: (MonadIO m, Random a) => (a, a) -> [m a]
randomListInf p = iterate (\_ -> randomRM p globalStdGen) (randomRM p globalStdGen)


-- Generates and prints a random list of floats in [0..1] of the given length N
printRandomList :: Int -> IO ()
printRandomList n = randomList n >>= print


-- Check if a list is sorted in the ascending order
isSorted :: Ord a => [a] -> Bool
isSorted xs = case xs of
  (x:y:xs) -> x <= y && isSorted (y:xs)
  _ -> True
