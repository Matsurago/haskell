import Test.Hspec
import Test.QuickCheck
import BinaryTree

main :: IO ()
main = hspec $ do
  describe "Binary Tree Test" $ do

    it "makes Tree from empty list" $ do
      fromList ([] :: [Int]) `shouldBe` Leaf

    it "makes Tree from a list" $ do
      fromList [5, 2, 4, 7, 8] `shouldBe` (Node 5 (Node 2 Leaf (Node 4 Leaf Leaf)) (Node 7 Leaf (Node 8 Leaf Leaf)))

    it "traverses in order" $ do
      inorder (fromList [4, 6, 7, 2, 5, 3, 1]) `shouldBe` [1, 2, 3, 4, 5, 6, 7]

    it "traverses pre-order" $ do
      preorder (fromList [4, 6, 7, 2, 5, 3, 1]) `shouldBe` [4, 2, 1, 3, 6, 5, 7]

    it "traverses post-order" $ do
      postorder (fromList [4, 6, 7, 2, 5, 3, 1]) `shouldBe` [1, 3, 2, 5, 7, 6, 4]
