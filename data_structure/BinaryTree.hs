module BinaryTree where

-- Binary Tree --
data Tree a = Leaf | Node a (Tree a) (Tree a) deriving (Show, Eq)


-- Note: makes sense only for a general binary tree.
-- For BST, it may violate the invariant.
instance Functor Tree where
    fmap _ Leaf = Leaf
    fmap f (Node x left right) = 
        Node (f x) (fmap f left) (fmap f right)


instance Semigroup a => Semigroup (Tree a) where
    (<>) Leaf Leaf = Leaf
    (<>) x Leaf = x
    (<>) Leaf x = x
    (<>) (Node x right left) (Node x' right' left') =
        Node (x <> x') (right <> right') (left <> left')


instance Semigroup a => Monoid (Tree a) where
    mappend = (<>)
    mempty = Leaf


insert :: Ord a => Tree a -> a -> Tree a
insert Leaf value = Node value Leaf Leaf
insert (Node x left right) value
    | value <= x = Node x (insert left value) right
    | otherwise = Node x left (insert right value) 


preorder :: Tree a -> [a]
preorder Leaf = []
preorder (Node x left right) = 
    [x] ++ preorder left ++ preorder right


inorder :: Tree a -> [a]
inorder Leaf = []
inorder (Node x left right) = 
    inorder left ++ [x] ++ inorder right


postorder :: Tree a -> [a]
postorder Leaf = []
postorder (Node x left right) = 
    postorder left ++ postorder right ++ [x]


foldrT :: (a -> b -> b) -> b -> Tree a -> b
foldrT f acc = foldr f acc . inorder


fromList :: Ord a => [a] -> Tree a
fromList = foldl insert Leaf


sampleTree :: Tree Int
sampleTree = Node 2 (Node 1 Leaf Leaf) (Node 3 Leaf Leaf)
