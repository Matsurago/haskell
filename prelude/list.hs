import Data.List

main :: IO()
main = do
    print $ nub [1,2,1,3,1,3,3,4]   -- remove duplicates
    print $ group [1,1,1,2,2,1,1,3,3]  -- groups adjacent elements: [[1,1,1],[2,2],[1,1],[3,3]]
    print $ sort [4,3,1,2,0,5]  -- sort ascending
    print $ tails [1,2,3]   -- all sublists: [[1,2,3],[2,3],[3],[]]
    print $ "test" `isPrefixOf` "tests are running"  -- true
    print $ "believ" `isInfixOf` "unbelievable"      -- true
    print $ any (<0) [1..5]   -- false
    print $ find (> 'k') "crab"   -- finds first predicate match: (Just 'r')
