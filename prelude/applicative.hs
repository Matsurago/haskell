import Control.Applicative (liftA3)

main :: IO()
main = do
    -- apply a function to multiple arguments in context
    let add x y z = x + y + z
    print $ liftA3 add (Just 3) (Just 4) (Just 5)
