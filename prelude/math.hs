main :: IO()
main = do 
    print $ log 2.718  -- natural log
    print $ logBase 10 100  -- log with custom base
    
    -- Cartesian coordinates to polar
    let x = -1
    let y = -(sqrt 3)
    print $ sqrt (x^2 + y^2)  -- r
    print $ atan2 y x  -- phi
