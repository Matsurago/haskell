import Data.Ratio

main :: IO()
main = do
    let x = 1 % 2   -- 1 / 2
    let y = 1 % 3   -- 1 / 3
    print $ x + y
    print $ fromRational x   -- to floating point number (0.5)
    print $ toRational 0.6   -- find rational close to this floating point number
