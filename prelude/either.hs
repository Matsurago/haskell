-- 'Either a b' can hold alternative values for two different types
-- Often used to represent either a result or an error

getServerId :: String -> Either String Int
getServerId "test.com" = Right 1
getServerId "example.com" = Right 2
getServerId "admin.com" = Left "Access restricted"
getServerId _ = Left "Unknown server"

main = do
    print $ getServerId "example.com"
    print $ getServerId "admin.com"
    
    -- Either is also a functor
    print $ fmap (\id -> "Server ID: " ++ (show id)) $ getServerId "test.com"
