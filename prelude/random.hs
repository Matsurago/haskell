import System.Random

main :: IO()
main = do
  roll <- randomRIO (1 :: Int, 6 :: Int)
  putStrLn (show roll)
