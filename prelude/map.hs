-- Maps are implemented using binary trees,
-- unlike other languages that employ hash tables.

import Data.Maybe
import qualified Data.Map as Map

main :: IO()
main = do
  -- WARNING: if duplicate keys are found, only the last one is retained
  let hotelClients = Map.fromList [(7, "John"), (13, "Bond"), (13, "James"), (35, "Helga")]
  print $ hotelClients    -- (13, "Bond") is discarded
  print $ Map.lookup 35 hotelClients
  print $ Map.lookup 44 hotelClients
  
  -- NOTE: Map.insert results in a new map
  let updatedHotelClients = Map.insert 44 "Chan" hotelClients
  print $ Map.lookup 44 updatedHotelClients

  -- fromListWith allows specifying merge function for duplicate values
  let phoneNumbersData = [("Ada", "123-4567"), ("Ada", "989-0077"), ("Bob", "233-1113")]
  let phoneNumbers = Map.fromListWith (++) $ map (\(user, phone) -> (user, [phone])) phoneNumbersData
  print $ phoneNumbers

  print $ Map.size phoneNumbers   -- number of entries (2)

  -- Map.map return a new map with a function applied to each entry
  print $ Map.map (\phones -> length phones) phoneNumbers
