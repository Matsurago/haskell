{-# LANGUAGE OverloadedStrings #-}
import qualified Data.Text as T

theWords :: T.Text
theWords = "Test\nData"

main :: IO()
main = do
  print (T.lines theWords)  -- split on \n
  print (T.words "A white cat\nand a brown dog") -- split on any whitespace
  print (T.splitOn "," "123,456,789")  -- split on any text
  print (T.unwords ["Some", "Text"]) -- like String.join() in Java (on whitespace)
  print (T.unlines ["Some", "Text"]) -- like String.join() in Java (on \n)
  print (T.intercalate ":" ["user", "password", "123456"]) -- like String.join() on any text
  print (mconcat ["test", " ", "data"])  -- concatenation
