import Data.Char

main :: IO()
main = do
    print $ toUpper 'a'
    print $ chr 97       -- int to char
    print $ ord 'a'      -- char to int
    print $ digitToInt 'f'    -- hex digit to int, case insensitive
    print $ isDigit '4'       -- only for 0..9
