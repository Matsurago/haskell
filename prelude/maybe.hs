import Data.Maybe

-- Demonstrates how to unwrap Maybe (Haskell's Optional) type.
-- fromMaybe is similar to Java's Optional.getOrElse()
-- fromMaybe :: a -> Maybe a -> a
main = do
    let x = Just 3
    let y = Nothing
    print $ fromMaybe 0 x -- default value is 0
    print $ fromMaybe 0 y

    -- apply a function to a Maybe value or return a default
    print $ maybe "" show (Just 3)

    -- Maybe is a functor
    print $ fmap (+ 3) (Just 2)
