import System.Environment
import System.Random
import Control.Monad
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as BC

-- Glitching images by sorting a random sequence of bytes

randomSortSelection :: BC.ByteString -> IO BC.ByteString
randomSortSelection bytes = do
    let sectionSize = 25
    let bytesLength = BC.length bytes
    start <- randomRIO (0, bytesLength - sectionSize)
    return (sortSelection start sectionSize bytes)

sortSelection :: Int -> Int -> BC.ByteString -> BC.ByteString
sortSelection startSelection size bytes = 
    mconcat [before, changed, after]
    where 
        (before, rest) = BC.splitAt startSelection bytes
        (selection, after) = BC.splitAt size rest
        changed = BC.reverse (BC.sort selection)

-- Glitching images by altering one byte        

randomReplaceByte :: BC.ByteString -> IO BC.ByteString
randomReplaceByte bytes = do
    let length = BC.length bytes
    offset <- randomRIO (1, length)
    charValue <- randomRIO (0, 255)
    return (replaceByte offset charValue bytes)

replaceByte :: Int -> Int -> BC.ByteString -> BC.ByteString
replaceByte offset charValue bytes = 
    mconcat [before, newChar, after]
    where 
        (before, rest) = BC.splitAt offset bytes
        after = BC.drop 1 rest
        newChar = intToBC charValue 

intToBC :: Int -> BC.ByteString
intToBC value = BC.pack [intToChar value]

intToChar :: Int -> Char
intToChar value = 
    toEnum safeValue
    where safeValue = value `mod` 255

main :: IO ()
main = do
    args <- getArgs
    
    let filename = head args
    imageFile <- BC.readFile filename
    
    let transforms = [randomReplaceByte, randomSortSelection, randomReplaceByte, randomSortSelection, randomReplaceByte]
    outputFile <- foldM (\bytes f -> f bytes) imageFile transforms
    
    let outputFilename = mconcat ["modified_", filename]
    BC.writeFile outputFilename outputFile
    print "all done"
