-- Using non-lazy IO and File IO helper functions --
import qualified Data.Text.IO as TextIO

main :: IO ()
main = do
    input <- TextIO.readFile "text.txt"
    TextIO.putStrLn input
    TextIO.writeFile "output.txt" input
