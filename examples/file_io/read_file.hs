import System.IO

main :: IO ()
main = do
    f <- openFile "text.txt" ReadMode
    out <- openFile "output.txt" WriteMode

    contents <- hGetContents f   -- is a lazy function, so we cannot close the handle yet
    hPutStrLn out contents
    
    hClose f
    hClose out

    putStrLn "done!"
