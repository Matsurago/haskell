-- Emulate fields: storing data using properties
cup ounces = \message -> message ounces

-- a Getter
getOunces aCup = aCup (\ounces -> ounces)

-- mutate data (in fact, creating a new object)
drink aCup ouncesToDrink =
    if diff >= 0
    then cup (ounces - ouncesToDrink)
    else cup 0
    where 
        ounces = getOunces aCup
        diff   = ounces - ouncesToDrink

-- drink many times (ex: drinkMany coffeecup [5,10,3])
drinkMany aCup times = foldl drink aCup times

-- a boolean property
isEmpty aCup = getOunces aCup == 0


-- predefined objects
coffeecup = cup 100
