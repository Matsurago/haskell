-- From "Programming in Haskell" by Graham Hutton
import Data.Char (chr, ord, isLower, toLower)

letterToIndex :: Char -> Int
letterToIndex c = ord c - ord 'a'

indexToLetter :: Int -> Char
indexToLetter i = chr $ ord 'a' + i

shift :: Int -> Char -> Char
shift i c 
    | isLower c' = indexToLetter $ (letterToIndex c' + i) `mod` 26
    | otherwise = c'
    where c' = toLower c

encode :: Int -> String -> String
encode i = map (shift i)


-- cracking the cypher
-- English letter frequency table
freqTable :: [Float]
freqTable = 
    [
        8.1, 1.5, 2.8, 4.2, 12.7, 2.2, 2.0, 6.1, 7.0,
        0.2, 0.8, 4.0, 2.4, 6.7, 7.5, 1.9, 0.1, 6.0,
        6.3, 9.0, 2.8, 1.0, 2.4, 0.2, 2.0, 0.1
    ]

letters :: [Char]
letters = ['a'..'z']

percent :: Int -> Int -> Float
percent x y = (fromIntegral x / fromIntegral y) * 100

-- Number of letters in a String
numLetters :: String -> Int
numLetters = length . filter (flip elem letters)

-- Number of occurences of character c in a String
countChar :: Char -> String -> Int
countChar c = length . filter (== c)

-- Calculates frequencies for any given String
freq :: String -> [Float]
freq s = [percent (countChar c s') n | c <- letters]
    where 
        s' = map toLower s
        n  = numLetters s'

-- χ2 (chi-squared) statistic can be used to calculate 
-- how close observed frequencies to expected;
-- lower values are better
chisqr :: [Float] -> [Float] -> Float
chisqr obs exp = sum [(o - e)^2 / e | (o, e) <- zip obs exp]

-- Rotates a list of values to the right
rotate :: Int -> [a] -> [a]
rotate i xs = drop i xs ++ take i xs

-- Crack the cypher!
-- note: doesn't guess correctly if a string is short or 
--       has unusual letter distribution
decode :: String -> String
decode s = encode (-key) s
    where
        key = snd $ minimum [(f i, i) | i <- [0..25]]
        f i = chisqr (rotate i obs) freqTable 
        obs = freq s

-- test
plainText = decode "kdvnhoo lv ixq"
