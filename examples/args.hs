-- Read program arguments --
import System.Environment

main :: IO()
main = do
    args <- getArgs  -- getArgs :: IO [String]
    mapM_ putStrLn args  -- map <IO Action> <List>, throw out results
