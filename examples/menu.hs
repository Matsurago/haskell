-- Example of a menu
import Control.Monad (when)

options :: [String]
options = ["Option 1", "Option 2", "Option 3", "Exit"]

isExitOption :: Int -> Bool
isExitOption index = (index == length options)

enumerateOptions :: [String] -> [String]
enumerateOptions = map (\(index, text) -> show index ++ ") " ++ text) . zip [1..]

executeOption :: Int -> [String] -> IO ()
executeOption index options 
    | selectedIndex >= 0 && selectedIndex <= maxIndex = putStrLn $ (options !! (index - 1)) ++ " is executed."
    | otherwise = putStrLn "No such option."
    where 
        maxIndex = (length options) - 1
        selectedIndex = index - 1

main = do
    mapM_ putStrLn $ enumerateOptions options
    userInput <- fmap (\x -> read x :: Int) getLine
    executeOption userInput options
    
    when (not . isExitOption $ userInput) main
