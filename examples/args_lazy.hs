-- Reads numbers and calculates the sum --
import System.Environment

toInts :: String -> [Int]
toInts = map read . lines  -- 'lines' splits on \n

main :: IO()
main = do
    userInput <- getContents  -- reads input as [Char]
    let numbers = toInts userInput
    print (sum numbers)
