module Main where

import qualified Data.Map as M

import Test.QuickCheck (Gen, Property, quickCheck, elements, forAll)

import Morse

-- helper methods

validChars :: [Char]
validChars = M.keys letterToMorse

validMorse :: [Morse]
validMorse = M.elems letterToMorse

genChar :: Gen Char
genChar = elements validChars

genMorse :: Gen Morse
genMorse = elements validMorse

-- tests

testConvertBackAndForth :: Property
testConvertBackAndForth = 
    forAll genChar 
    (\c -> ((encodeChar c) >>= decodeChar) == Right c)

main :: IO ()
main = quickCheck testConvertBackAndForth
