# Morse

This sample app that converts English text into the Morse code
is adapted from the book "Haskell Programming from First Principles"
by C. Allen and J. Moronuki.

## Run
```
cabal run morse -- to
```

## Test
```
cabal test
```
