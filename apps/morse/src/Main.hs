module Main where

import Control.Monad (forever, when)
import Data.List (intercalate)
import System.Environment (getArgs)
import System.Exit (exitFailure, exitSuccess)
import System.IO (hGetLine, hIsEOF, stdin)
import Text.Printf (printf)

import Morse (encode, decode)

convertToMorse :: IO ()
convertToMorse = forever $ do
    done <- hIsEOF stdin
    when done exitSuccess

    line <- hGetLine stdin

    case (encode line) of 
        Right s -> printf . intercalate " " $ s
        Left e  -> printf "[ERROR] %s" e

    putStrLn ""


convertFromMorse :: IO ()
convertFromMorse = forever $ do
    done <- hIsEOF stdin
    when done exitSuccess

    line <- hGetLine stdin

    case (decode . words $ line) of 
        Right s -> printf s
        Left e  -> printf "[ERROR] %s" e

    putStrLn ""


-- Prints app usage and exits
argError :: IO ()
argError = do
    putStrLn "Usage: morse [from|to]"
    exitFailure


-- Usage from CLI: morse [from|to]
-- From GHCi, can specify args with e.g. ':set args from'
-- Sample input (from): ... --- ...
main :: IO ()
main = do
    mode <- getArgs
    case mode of
        [arg] -> case arg of
            "from" -> convertFromMorse
            "to" -> convertToMorse
            _ -> argError
        _ -> argError
