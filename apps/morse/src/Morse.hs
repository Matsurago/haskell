-- Adapted from the book "Haskell Programming from First Principles"

module Morse (
    Morse,
    letterToMorse,
    morseToLetter,
    encodeChar,
    encode,
    decodeChar,
    decode
) where

import qualified Data.Map as M

type Morse = String
type ParseError = String

-- A map from ASCII char to the corresponding Morse code symbol
letterToMorse :: M.Map Char Morse
letterToMorse = M.fromList [
    ('a', ".-"),
    ('b', "-..."),
    ('c', "-.-."),
    ('d', "-.."),
    ('e', "."),
    ('f', "..-."),
    ('g', "--."),
    ('h', "...."),
    ('i', ".."),
    ('j', ".---"),
    ('k', "-.-"),
    ('l', ".-.."),
    ('m', "--"),
    ('n', "-."),
    ('o', "---"),
    ('p', ".--."),
    ('q', "--.-"),
    ('r', ".-."),
    ('s', "..."),
    ('t', "-"),
    ('u', "..-"),
    ('v', "...-"),
    ('w', ".--"),
    ('x', "-..-"),
    ('y', "-.--"),
    ('z', "--.."),
    ('1', ".----"),
    ('2', "..---"),
    ('3', "...--"),
    ('4', "....-"),
    ('5', "....."),
    ('6', "-...."),
    ('7', "--..."),
    ('8', "---.."),
    ('9', "----."),
    ('0', "-----")
    ]

-- A reverse map from a Morse code symbol to the corresponding ASCII char
morseToLetter :: M.Map Morse Char
morseToLetter = M.foldrWithKey (flip M.insert) M.empty letterToMorse

-- Converts a single ASCII char to its Morse code symbol
-- encodeChar 's' = Right '...'
encodeChar :: Char -> Either ParseError Morse
encodeChar c = 
    case (M.lookup c letterToMorse) of
        Nothing    -> Left $ "Unconvertible symbol: '" ++ [c] ++ "'"
        Just morse -> Right morse

-- Converts a list of ASCII chars (i.e. String) to the list of corresponding Morse symbols
-- Ignores whitespace
encode :: String -> Either ParseError [Morse]
encode = sequence . map encodeChar . filter (/= ' ')

-- Decodes a Morse symbol into the corresponding ASCII char
-- morseToChar "..." = Right 's'
decodeChar :: Morse -> Either ParseError Char
decodeChar morse = 
    case (M.lookup morse morseToLetter) of 
        Nothing -> Left $ "Unconvertible morse code: '" ++ morse ++ "'"
        Just c  -> Right c

-- Decodes a list of Morse symbols into the corresponding ASCII String
decode :: [Morse] -> Either ParseError String
decode = traverse decodeChar
